# 夏日风扇（小程序）

主要用于学习小程序的开发和部署流程

## 实现思路

- 使用 CSS3 的 animation 实现风叶旋转
- 使用 animation-duration 来控制旋转速度

## 截图

![夏日风扇](./screenshot.png)

## todo 

- 兼容性问题