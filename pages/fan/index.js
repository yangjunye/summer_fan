// pages/fan/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fenyeClass: 'fengye stop',
    fenyeStyle: 'animation-play-state: paused;'
  },
  strong () {
    this.setData({
      fenyeStyle: 'animation-duration: 0.01s;'
      // fenyeClass: 'fengye speed0'
    })
  },
  middle() {
    this.setData({
      fenyeStyle: 'animation-duration: 0.1s;'
      // fenyeClass: 'fengye speed1'
    })
  },
  weak() {
    this.setData({
      fenyeStyle: 'animation-duration: 1s;'
      // fenyeClass: 'fengye speed2'
    })
  },    
  close() {
    this.setData({
      fenyeStyle: 'animation-play-state: paused;'
      // fenyeClass: 'fengye stop'
    })
  },   
})